package pl.hackaton.wunderbar;

import android.app.Application;

import io.relayr.android.RelayrSdk;
import timber.log.Timber;

/**
 * @author Dawid Szydlo
 */
public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        new RelayrSdk.Builder(this).build();
        Timber.plant(new Timber.DebugTree());
    }
}
