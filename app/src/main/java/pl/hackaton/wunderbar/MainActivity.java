package pl.hackaton.wunderbar;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import io.relayr.android.RelayrSdk;
import io.relayr.java.model.Transmitter;
import io.relayr.java.model.TransmitterDevice;
import io.relayr.java.model.User;
import pl.hackaton.wunderbar.api.Manager;
import pl.hackaton.wunderbar.neural.EncogNetwork;
import pl.hackaton.wunderbar.opencv.ImageManipulation;
import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import timber.log.Timber;

/**
 * Numbers
 */
public class MainActivity extends AppCompatActivity {

    @InjectView(R.id.toolbar)
    Toolbar toolbar;


    @InjectView(R.id.img)
    ImageView img;

    @InjectView(R.id.textView)
    TextView mTextView;

    EncogNetwork encogNetwork;

    Subscription loginSubs, imageStreamSubs, lightSubs, gyroSubs;

    ImageManipulation imageManipulation;

    private BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status) {
                case LoaderCallbackInterface.SUCCESS: {
                    Log.i("OpenCV", "OpenCV loaded successfully");
                    imageManipulation = new ImageManipulation();
                }
                break;
                default: {
                    super.onManagerConnected(status);
                }
                break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);
        setSupportActionBar(toolbar);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!RelayrSdk.isUserLoggedIn()) {
            loginSubs = RelayrSdk.logIn(this)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Observer<User>() {
                        @Override
                        public void onCompleted() {
                            Timber.d("MainActivity compl");
                        }

                        @Override
                        public void onError(Throwable e) {
                            Toast.makeText(MainActivity.this,
                                    R.string.unsuccessfully_logged_in, Toast.LENGTH_SHORT).show();
                            Timber.d(e, "MainActivity");
                        }

                        @Override
                        public void onNext(User user) {
                            Timber.d("MainActivity next" + user);
                            Toast.makeText(MainActivity.this,
                                    R.string.successfully_logged_in, Toast.LENGTH_SHORT).show();
                            loadTemperatureDevice();
                        }
                    });
            return;
        } else {
            loadTemperatureDevice();
        }

        OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION_2_4_9, this, mLoaderCallback);

        encogNetwork = new EncogNetwork(this);
        mTextView.setText("");

        imageStreamSubs = Manager.getInstance(this).getBitmapStream()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Bitmap>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e, "MainActivity encog");
                    }

                    @Override
                    public void onNext(Bitmap bitmap) {
                        imageManipulation.setImage(bitmap);
                        imageManipulation.applyGrayscale();
                        String str = encogNetwork.run(imageManipulation.getImage());
                        img.setImageBitmap(imageManipulation.getImage());
                        Timber.d("MainActivity encog %s", str);
                        mTextView.setText(String.format("%s%s", mTextView.getText().toString(), str));
                    }
                });

    }

    private void loadTemperatureDevice() {
        //Check if cache is loaded before trying to use it
        //Every model can also be retrieved with RelayrSdk.getDeviceModelsApi().getDeviceModelById()
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                RelayrSdk.getUser()
                        .flatMap(new Func1<User, Observable<List<Transmitter>>>() {
                            @Override
                            public Observable<List<Transmitter>> call(User user) {
                                return user.getTransmitters();
                            }
                        })
                        .flatMap(new Func1<List<Transmitter>, Observable<List<TransmitterDevice>>>() {
                            @Override
                            public Observable<List<TransmitterDevice>> call(List<Transmitter> transmitters) {
                                if (transmitters.isEmpty()) {
                                    return Observable.from(new ArrayList<List<TransmitterDevice>>());
                                }
                                return RelayrSdk.getRelayrApi().getTransmitterDevices(transmitters.get(0).getId());
                            }
                        })
                        .subscribe(new Observer<List<TransmitterDevice>>() {
                            @Override
                            public void onCompleted() {

                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onNext(List<TransmitterDevice> transmitters) {
                                for (TransmitterDevice device : transmitters) {
                                    if (device.getName().equals("light")) {
                                        lightSubs = Manager.getInstance(MainActivity.this)
                                                .light(device.subscribeToCloudReadings());
                                    } else if (device.getName().equals("gyroscope")) {
                                        gyroSubs = Manager.getInstance(MainActivity.this)
                                                .gyro(device.subscribeToCloudReadings(), getApplication());
                                    }
                                }
                            }
                        });
            }
        }, 2000);
    }

    @Override
    protected void onDestroy() {
        if (lightSubs != null && !lightSubs.isUnsubscribed()) {
            lightSubs.unsubscribe();
            lightSubs = null;
        }
        if (loginSubs != null && !loginSubs.isUnsubscribed()) {
            loginSubs.unsubscribe();
            loginSubs = null;
        }
        if (imageStreamSubs != null && !imageStreamSubs.isUnsubscribed()) {
            imageStreamSubs.unsubscribe();
            imageStreamSubs = null;
        }
        if (gyroSubs != null && !gyroSubs.isUnsubscribed()) {
            gyroSubs.unsubscribe();
            gyroSubs = null;
        }
        encogNetwork.finalize();
        RelayrSdk.logOut();
        super.onDestroy();
    }

    @OnClick(R.id.fab)
    void click() {
        mTextView.setText("");
    }
}
