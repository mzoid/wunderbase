package pl.hackaton.wunderbar.api;

import com.google.gson.internal.LinkedTreeMap;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

import io.relayr.java.model.action.Reading;
import rx.Observable;
import rx.Observer;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.subjects.PublishSubject;
import timber.log.Timber;

/**
 * @author Dawid Szydlo
 */
public class Manager {

    static Manager INSTANCE;

    Subscription mSubscription;

    boolean isRecording;

    private PublishSubject<List<Point>> pointList;

    private PublishSubject<Bitmap> bitmapStream;

    private Manager(Context context) {
        pointList = PublishSubject.create();
        bitmapStream = PublishSubject.create();

        mSubscription = pointList
                .subscribe(new Observer<List<Point>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e, "Manager");
                    }

                    @Override
                    public void onNext(List<Point> points) {
                        int minX, minY, maxX, maxY;

                        if (points != null && points.size() > 0) {
                            for (Point point : points) {
                                point.y = -point.y;
                            }

                            minX = maxX = points.get(0).x;
                            minY = maxY = points.get(0).y;

                            for (Point point : points) {
                                minX = Math.min(minX, point.x);
                                minY = Math.min(minY, point.y);
                                maxX = Math.max(maxX, point.x);
                                maxY = Math.max(maxY, point.y);
                            }
                            double xFactor = 180. / ((double) maxX - (double) minX);
                            double yFactor = 180. / ((double) maxY - (double) minY);
                            for (Point point : points) {
                                point.x = (int) ((point.x - minX) * xFactor) + 10;
                                point.y = (int) ((point.y - minY) * yFactor) + 10;
                            }

                            Bitmap bitmap = Bitmap.createBitmap(200, 200, Bitmap.Config.ARGB_8888);
                            Canvas canvas = new Canvas(bitmap);
                            canvas.drawColor(Color.WHITE);
                            Paint color = new Paint();
                            color.setColor(Color.BLACK);
                            color.setStrokeWidth(25);

                            for (int i = 1; i < points.size(); i++) {
                                canvas.drawLine(points.get(i - 1).x, points.get(i - 1).y,
                                        points.get(i).x, points.get(i).y, color);
                            }

                            canvas.save();
                            bitmapStream.onNext(bitmap);
                        }
                    }
                });

        Observable.just(1)
                .delay(2000, TimeUnit.MILLISECONDS)
                .doOnNext(new Action1<Integer>() {
                    @Override
                    public void call(Integer integer) {
                        Point[] array = {
                                new Point(-1, 0),
                                new Point(0, 1),
                                new Point(0, -2)
                        };
                        pointList.onNext(Arrays.asList(array));
                    }
                })
                .delay(2000, TimeUnit.MILLISECONDS)
                .doOnNext(new Action1<Integer>() {
                    @Override
                    public void call(Integer integer) {
                        Point[] array = {
                                new Point(0, 2),
                                new Point(0, 0),
                                new Point(-1, 1),
                                new Point(1, 1)
                        };
                        pointList.onNext(Arrays.asList(array));
                    }
                })
                .delay(2000, TimeUnit.MILLISECONDS)
                .doOnNext(new Action1<Integer>() {
                    @Override
                    public void call(Integer integer) {
                        Point[] array = {
                                new Point(20, 0),
                                new Point(30, 0),
                                new Point(25, -20),
                        };
                        pointList.onNext(Arrays.asList(array));
                    }
                })
                .subscribe();
    }

    public static Manager getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new Manager(context);
        }
        return INSTANCE;
    }

    public Observable<Bitmap> getBitmapStream() {
        return bitmapStream.asObservable();
    }

    public Subscription light(Observable<Reading> stream) {
        return stream
                .subscribe(new Observer<Reading>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e, "Manager light");
                    }

                    @Override
                    public void onNext(Reading reading) {
                        if (reading.meaning.equals("proximity")) {
                            isRecording = (double) reading.value > 500.;
                            Timber.e("Manager light %s", isRecording);
                        }
                    }
                });
    }

    public Subscription gyro(Observable<Reading> stream, final Context context) {
        return stream
                .filter(new Func1<Reading, Boolean>() {
                    @Override
                    public Boolean call(Reading reading) {
                        return reading.meaning.equals("acceleration");//angularSpeed
                    }
                })
                .subscribeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Reading>() {
                    boolean lastReading = false;

                    ArrayList<Point> list = new ArrayList<Point>();

                    @Override
                    public void onCompleted() {
                        Timber.e("completed!");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Timber.e(e, "error");
                    }

                    @Override
                    public void onNext(Reading reading) {
                        if (isRecording) {
                            if (list.size() == 0) {
                                list.add(new Point(0, 0));
                            } else {
                                LinkedTreeMap<String, Double> o = (LinkedTreeMap<String, Double>) reading.value;
                                list.add(new Point(list.get(list.size() - 1).x + (int)(o.get("x")*1000),
                                        list.get(list.size() - 1).y + (int)(o.get("z")*1000-1000)));
//                                list.add(new Point(reading.value))
                            }
//                            list.add(reading);

                        }
                        if (lastReading && !isRecording) {
                            pointList.onNext(list);
                            list = new ArrayList<>();
                        }
                        Timber.d(reading.toString());
                        lastReading = isRecording;
                    }
                });
    }
}
