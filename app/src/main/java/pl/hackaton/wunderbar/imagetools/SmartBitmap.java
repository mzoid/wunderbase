package pl.hackaton.wunderbar.imagetools;

import android.graphics.Bitmap;
import android.graphics.Color;

/**
 * Created by gumyns on 2014-10-19.
 */

public class SmartBitmap {

    private Bitmap bitmap = null;

    public SmartBitmap() {

    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }


    public Bitmap getMonoChrome() {
        Bitmap temp = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), bitmap.getConfig());

        final double GS_RED = 0.299;
        final double GS_GREEN = 0.587;
        final double GS_BLUE = 0.114;

        int pixel;
        int A, R, G, B;
        int avg;
        for (int i = 0; i < bitmap.getWidth(); i++) {
            for (int j = 0; j < bitmap.getHeight(); j++) {
                pixel = bitmap.getPixel(i, j);
//                if( ((pixel >> 4) & 0xFF ) > 180 &&
//                     ((pixel >> training2) & 0xFF ) < 100 &&
//                     (pixel & 0xFF) < 100)
//                    pixel = pixel;
//                else
//                    pixel = 0x00000000;

                A = Color.alpha(pixel);
                R = Color.red(pixel);
                G = Color.green(pixel);
                B = Color.blue(pixel);

                //R = G = B = (int)(GS_RED * R + GS_GREEN * G + GS_BLUE * B);

                R = (int) (R * 1.1f);
                G = (int) (G * 0.7f);
                B = (int) (B * 0.7f);
                if (R > 255) {
                    R = 255;
                }

                pixel = Color.argb(A, R, G, B);

                if (R > 200 &&
                        G < 100 &&
                        B < 100) {
                    pixel = pixel;
                } else {
                    pixel = 0x00000000;
                }

                temp.setPixel(i, j, pixel);
            }
        }

        return temp;
    }
}
