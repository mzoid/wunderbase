package pl.hackaton.wunderbar.opencv;

import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import android.graphics.Bitmap;
import android.util.Log;

import java.util.LinkedList;
import java.util.List;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;

/**
 * Created by gumyns on 2014-11-09.
 */
public class ImageManipulation {

    Mat image = null;

    private Mat grayscaleKernel;

    public ImageManipulation() {
        // Fill sepia kernel
        grayscaleKernel = new Mat(4, 4, CvType.CV_32F);
        grayscaleKernel.put(0, 0, /* R */0.189f, 0.189f, 0.189f, 0f);
        grayscaleKernel.put(1, 0, /* G */0.468f, 0.468f, 0.468f, 0f);
        grayscaleKernel.put(2, 0, /* B */0.343f, 0.343f, 0.343f, 0f);
        grayscaleKernel.put(3, 0, /* A */0.000f, 0.000f, 0.000f, 1f);
    }

    public Bitmap getImage() {
        Bitmap bitmap = Bitmap.createBitmap(image.width(), image.height(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(image, bitmap);
        return bitmap;
    }

    public void setImage(final Bitmap bitmap) {
        if (image != null) {
            image.release();
        }
        image = new Mat(bitmap.getWidth(), bitmap.getHeight(), CvType.CV_8U);
        Utils.bitmapToMat(bitmap.copy(Bitmap.Config.ARGB_8888, true), image);
    }

    private Bitmap getImage(Mat src) {
        Bitmap bitmap = Bitmap.createBitmap(src.width(), src.height(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(src, bitmap);
        return bitmap;
    }

    public void applyGrayscale() {
        applyThreshold(image, 150);
        Imgproc.resize(image, image, new Size(32, 32));
        Imgproc.cvtColor(image, image, Imgproc.COLOR_RGBA2GRAY);
        applyThreshold(image, 250);
        Imgproc.resize(image, image, new Size(32, 32));
    }

    public void applyGrayscale(Mat src) {
        Imgproc.cvtColor(src, src, Imgproc.COLOR_RGB2GRAY);
    }

    private double getThresh(Mat src) {
//        double thresh = 0.0;
//        int count = 0;
//        for(int i = 0; i < src.cols(); i++){
//            for(int j = 0; j < src.rows(); j++) {
//                if(src.get(j,i)[0] < 160.0) {
//                    thresh += src.get(j,i)[0];
//                    count++;
//                }
//            }
//        }
//        Log.d("Thresh double", (thresh / (double)count) + "");
        //  130ms vs 2ms but upper was a bit better

        Scalar scalar = Core.mean(src);
//        Log.d("Thresh scalar", scalar.toString());
//
        return scalar.val[0] * 0.5;
    }

    public void applyThreshold(Mat src, double level) {
        Imgproc.medianBlur(src, src, 3);
        Imgproc.threshold(src, src, level, 255.0, Imgproc.THRESH_BINARY);
    }

    public double[] getStatisticForCols(Mat src) {
        Mat col;
        double[] cols = new double[src.cols()];

        for (int i = 0; i < cols.length - 1; i++) {
            col = src.submat(0, src.rows(), i, i + 1);
            cols[i] = 255 - Core.mean(col).val[0];
        }
        return cols;
    }

    public Mat getStatisticImageForCols(Mat src) {
        double[] cols = getStatisticForCols(src);
        // Draw lines for histogram points
        Mat histImage = new Mat(src.rows(), src.cols(), CvType.CV_8U, new Scalar(255, 255, 255));

        for (int i = 0; i < cols.length; i++) {
            Point p1 = new Point(i, 0);
            Point p2 = new Point(i, cols[i]);
            if (cols[i] > 0.0) {
                Core.line(
                        histImage,
                        p1,
                        p2,
                        new Scalar(0, 0, 0),
                        1, 8, 0);
            }
            Log.d("histogram", p1.toString() + ", " + p2.toString());
        }

        return histImage;
    }

    public double[] getStatisticForRows(Mat src) {
        Mat row;
        double[] rows = new double[src.rows()];

        for (int i = 0; i < rows.length; i++) {
            row = src.submat(i, i + 1, 0, src.cols());
            rows[i] = 255 - Core.mean(row).val[0];
        }
        return rows;
    }

    public Mat getStatisticImageForRows(Mat src) {
        double[] rows = getStatisticForRows(src);
        // Draw lines for histogram points
        Mat histImage = new Mat(src.rows(), src.cols(), CvType.CV_8U, new Scalar(255, 255, 255));

        for (int i = 0; i < rows.length; i++) {
            Point p1 = new Point(0, i);
            Point p2 = new Point(rows[i], i);
            if (rows[i] > 0.0) {
                Core.line(
                        histImage,
                        p1,
                        p2,
                        new Scalar(0, 0, 0),
                        1, 8, 0);
            }
            Log.d("histogram", p1.toString() + ", " + p2.toString());
        }

        return histImage;
    }

    public List<Mat> getRectangles(Mat src) {
        List<Mat> list = new LinkedList<Mat>();
        double[] cols = getStatisticForCols(src);
        int start = 0,
                end = 0;
        for (int i = 0; i < cols.length; i++) {
            if (cols[i] > 0.0) {
                if (start == 0) {
                    start = i;
                } else {
                    end = i;
                }
            } else {
                if (start > 0 && end > 0) {
                    list.add(src.submat(0, src.rows(), start, end));
                }
                start = 0;
                end = 0;
            }
        }

        List<Mat> ret = new LinkedList<Mat>();

        for (Mat m : list) {
            double[] rows = getStatisticForRows(m);
            for (int i = 0; i < rows.length; i++) {
                if (rows[i] > 0.0) {
                    if (start == 0) {
                        start = i;
                    } else {
                        end = i;
                    }
                } else {
                    if (start > 0 && end > 0) {
                        ret.add(m.submat(start, end, 0, m.cols()));
                    }
                    start = 0;
                    end = 0;
                }
            }
            m.release();
        }
        list.clear();

        return ret;
    }


    private Mat getCircle(Mat src) {
        int r = 0;
        for (int i = 0; i < src.rows() / 2; i++) {
            if (src.get(i, src.cols() / 2)[0] < 120.0) {
                r++;
            } else {
                break;
            }
        }
        //Mat temp = src.submat(r, src.rows()-r, r, src.cols()-r);

        double radius = (double) (src.cols() / 2) - r - 4;
        Point p = new Point();
        Scalar white = new Scalar(255., 255., 255.);
        for (int i = 0; i <= src.cols() / 2; i++) {
            for (int j = 0; j <= src.rows() / 2; j++) {
                if (Math.sqrt(
                        (double) ((src.cols() / 2 - i) * (src.cols() / 2 - i) + (src.rows() / 2 - j) * (src.rows() / 2
                                - j))) > radius) {
                    p.x = j;
                    p.y = i;
                    Core.line(src, p, p, white, 1);
                    p.x = j;
                    p.y = src.cols() - i;
                    Core.line(src, p, p, white, 1);
                    p.x = src.rows() - j;
                    p.y = i;
                    Core.line(src, p, p, white, 1);
                    p.x = src.rows() - j;
                    p.y = src.cols() - i;
                    Core.line(src, p, p, white, 1);
                }
            }
        }

        return src;
    }

    public Observable<Mat> findCircles(final Mat img) {
        return Observable.create(new Observable.OnSubscribe<Mat>() {
            @Override
            public void call(Subscriber<? super Mat> subscriber) {
                Mat circles = new Mat();
                Imgproc.HoughCircles(img, circles, Imgproc.CV_HOUGH_GRADIENT,
                        4.0, img.rows() * 8,
                        300, 50,
                        20, 500);
                if (circles.cols() > 0) {
                    for (int x = 0; x < circles.cols(); x++) {

                        double vCircle[] = circles.get(0, x);
                        if (vCircle == null) {
                            break;
                        }

                        Point pt = new Point(Math.round(vCircle[0]), Math.round(vCircle[1]));
                        int radius = (int) Math.round(vCircle[2]);
                        image = img.submat((int) (pt.y - radius), (int) (pt.y + radius), (int) (pt.x - radius),
                                (int) (pt.x + radius));
                        subscriber.onNext(getCircle(
                                img.submat((int) (pt.y - radius), (int) (pt.y + radius), (int) (pt.x - radius),
                                        (int) (pt.x + radius))));

                    }
                } else {
                    subscriber.onError(new NullPointerException());
                }
                circles.release();
                subscriber.onCompleted();
            }
        });
    }


    public Observable<List<Bitmap>> processCircles() {
        final long time = System.currentTimeMillis();
        return Observable.create(new Observable.OnSubscribe<List<Bitmap>>() {
            @Override
            public void call(final Subscriber<? super List<Bitmap>> subscriber) {
                final Mat img = image.clone();
                applyGrayscale(img);

                Subscription subscription = findCircles(img)
                        .subscribe(new Subscriber<Mat>() {
                            @Override
                            public void onCompleted() {
                                img.release();
                            }

                            @Override
                            public void onError(Throwable e) {

                            }

                            @Override
                            public void onNext(Mat mat) {

                                Mat circle = mat.clone();

                                applyThreshold(circle, getThresh(circle));

                                List<Mat> rectangles = getRectangles(circle);

                                if (rectangles.size() < 2 || rectangles.size() > 3) {
                                    subscriber.onCompleted();
                                    return;
                                }

                                List<Bitmap> retList = new LinkedList<Bitmap>();

                                for (Mat m : rectangles) {
                                    Imgproc.resize(m, m, new Size(32, 32));
                                    applyThreshold(m, 250);
                                    retList.add(getImage(m));
                                    m.release();
                                }
                                Log.d("Time whole onNext", System.currentTimeMillis() - time + "");
                                subscriber.onNext(retList);
                            }
                        });
            }
        });
    }

    public void finalize() {
        if (image != null) {
            image.release();
        }
        image = null;
    }
}
