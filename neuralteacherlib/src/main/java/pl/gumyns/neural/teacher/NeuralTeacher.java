package pl.gumyns.neural.teacher;

import pl.gumyns.neural.teacher.neural.NeuralNetwork;

public class NeuralTeacher {

    public static void main(String[] args) throws Exception {
        System.out.println("In main");
        teachNetwork();
    }

    private static void teachNetwork() {
        NeuralNetwork network = new NeuralNetwork();
        network.finalize();
    }
}
