package pl.gumyns.neural.teacher.model;

import java.io.Serializable;

/**
 * Created by gumyns on 2014-12-13.
 */
public class NetworkData implements Serializable {

    char[] output;

    double[] network;

    public NetworkData() {
    }

    public char[] getOutput() {
        return output;
    }

    public void setOutput(String[] output) {
        char[] tab = new char[output.length];
        for (int i = 0; i < output.length; i++) {
            tab[i] = output[i].charAt(0);
        }
        this.output = tab;
    }

    public double[] getNetwork() {
        return network;
    }

    public void setNetwork(double[] network) {
        this.network = network;
    }

    public void clearNetwork() {
        network = null;
    }


}
