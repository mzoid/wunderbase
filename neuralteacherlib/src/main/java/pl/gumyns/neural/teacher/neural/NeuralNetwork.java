package pl.gumyns.neural.teacher.neural;

import org.encog.Encog;
import org.encog.engine.network.activation.ActivationSigmoid;
import org.encog.ml.data.MLData;
import org.encog.ml.data.MLDataPair;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.data.basic.BasicMLDataSet;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;
import org.encog.neural.networks.training.propagation.resilient.ResilientPropagation;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.LinkedList;
import java.util.List;

import javax.imageio.ImageIO;

import pl.gumyns.neural.teacher.model.NetworkData;

/**
 * Created by gumyns on 2014-11-08.
 */
public class NeuralNetwork {

    BasicNetwork network;

    List<double[]> trainingData = new LinkedList<double[]>();

    List<double[]> trainingIdeal = new LinkedList<double[]>();

    NetworkData networkData;

    public NeuralNetwork() {
        BufferedImage img = null;
        File[] files;
        double[] image;
        long time = System.currentTimeMillis();

        File file = new File("../TrainingData");
        String[] directories = file.list(new FilenameFilter() {
            @Override
            public boolean accept(File current, String name) {
                return new File(current, name).isDirectory();
            }
        });

        //
        networkData = new NetworkData();
        networkData.setOutput(directories);

        for (int i = 0; i < directories.length; i++) {
            file = new File("../TrainingData/" + directories[i]);
            files = file.listFiles(new FilenameFilter() {
                @Override
                public boolean accept(File current, String name) {
                    return new File(current, name).isFile();
                }
            });
            for (File f : files) {
                try {
                    image = new double[32 * 32];
                    img = new BufferedImage(32, 32,
                            BufferedImage.TYPE_BYTE_GRAY);
                    img.createGraphics().drawImage(ImageIO.read(f), 0, 0, null);
                    img.getRaster().getPixels(0, 0, img.getWidth(), img.getHeight(), image);
                    for (int j = 0; j < image.length; j++) {
                        image[j] = image[j] > 128. ? 1. : 0.;
                    }

                    trainingData.add(image);
                    trainingIdeal.add(NeuralHelper.getIdeal(directories.length, i));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        System.out.println("image loading: " + (System.currentTimeMillis() - time) + "ms");
        System.out.println("trainingData size: " + trainingData.size());
        System.out.println("trainingData size: " + trainingIdeal.size());

        time = System.currentTimeMillis();

        double[][] input = new double[trainingData.size()][];
        for (int i = 0; i < trainingData.size(); i++) {
            input[i] = trainingData.get(i);
        }
        trainingData.clear();

        double[][] ideal = new double[trainingIdeal.size()][];
        for (int i = 0; i < trainingIdeal.size(); i++) {
            ideal[i] = trainingIdeal.get(i);
        }
        trainingIdeal.clear();

        network = new BasicNetwork();
        // create a neural network, without using a factory
        network.addLayer(new BasicLayer(null, true, 32 * 32));
        network.addLayer(new BasicLayer(new ActivationSigmoid(), true, 128));
        network.addLayer(new BasicLayer(new ActivationSigmoid(), false, directories.length));
        network.getStructure().finalizeStructure();
        network.reset();
        // create training data
        MLDataSet trainingSet = new BasicMLDataSet(input, ideal);
        // train the neural network
        final ResilientPropagation train = new ResilientPropagation(network, trainingSet);
        int epoch = 1;
        do {
            train.iteration();
            System.out.println("Neural " + "Epoch #" + epoch + " Error:" + train.getError());
            epoch++;
        } while (train.getError() > 0.001 && epoch < 1000);
        train.finishTraining();
        // test the neural network
        System.out.println("Neural " + "Neural Network Results:");
        for (MLDataPair pair : trainingSet) {
            final MLData output = network.compute(pair.getInput());
            StringBuilder builder = new StringBuilder();
            builder.append("actual= ");
            for (int i = 0; i < output.getData().length; i++) {
                builder.append(String.format("%.2f", output.getData(i)) + ", ");
            }
            builder.append("ideal= ");
            for (int i = 0; i < pair.getIdeal().getData().length; i++) {
                builder.append(pair.getIdeal().getData(i) + ", ");
            }

            System.out.println("Neural " + builder.toString());
        }
        System.out.println("teaching: " + (System.currentTimeMillis() - time) + "ms");

        double[] networkArray = new double[network.encodedArrayLength()];
        network.encodeToArray(networkArray);
        networkData.setNetwork(networkArray);

        try {
            FileOutputStream fileOut = new FileOutputStream("../app/src/main/assets/neural.ser");
            ObjectOutputStream out = new ObjectOutputStream(fileOut);
            out.writeObject(networkData);
            out.close();
            fileOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void finalize() {
        Encog.getInstance().shutdown();
        try {
            super.finalize();
        } catch (Throwable throwable) {
        }
    }
}